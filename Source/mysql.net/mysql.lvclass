﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">1970943</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str"></Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">mysql.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../mysql.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,5!!!*Q(C=T:1^DBJ"%)5@FK6VSAH7YAK6/O1+_!A6_3?S##(E#H5&amp;D)9$%$GP%*%BW1=QA36L\=4YG[&lt;.LX@:Q*;WGRK'^[KLP_FO2CLNB@2=WWON?&lt;!68S8;XF#WP?SUP;`SW7553[&gt;_(&gt;PMBZ\\4&lt;-P?:2][:@J&gt;SHFLLH/@[XJQ8&lt;O&amp;Y2\`79Z8S[&lt;_XT=&amp;OEEY_"8^SRD\R`=8=;Z0T`R6Z@_]F"BN6L.`_,8D.8K8J_-Z?J"P^J0TV_?_-PF&lt;$9\8@`Z@/`TG,034P&gt;PPP/0X00^,RH.\'"@H*`ZC8PJUY\=P`IH\4((?XT^4`"`G^1JJ%1314BBZ?GWC:\IC:\IC:\IA2\IA2\IA2\IDO\IDO\IDO\IBG\IBG\IBG\IJ;-,8?B#:V73S:/*EK**A31:&amp;#78B#@B38A3(HYKY5FY%J[%*_%B21F0QJ0Q*$Q*$]/5]#1]#5`#E`"1KJ"E[?DQ*$S56]!4]!1]!5`!QZ1+?!+!9,+A=&amp;!%$!6G="0Q"$Q"$\=+?!+?A#@A#8CQ&amp;@!%0!&amp;0Q"0Q-+3M3B3;NK0$1REZ0![0Q_0Q/$S5FM0D]$A]$I`$QX2S?"Q?"]+:U#E/AJR"4I,TQ_&amp;R?0C3Q_0Q/$Q/D]/$68&lt;)S]KU.'V(B]@A-8A-(I0(Y+'%$"[$R_!R?!Q?SMLA-8A-(I0(Y'%K'4Q'D]&amp;DA"C4-LW-9M:!)]E1$"Y_Z&lt;29W;5I*&amp;:[`7PO$KLK!61^7+I(2P5AK'[Q[M;J&lt;IDK1KMOI/L#K$[Q[I/I!KJ/L&amp;J1.6%&lt;LGNC15S*#4%E"E3@["(&gt;&gt;OA`4NRM.FKPVVIM&amp;JJ/JZJ-*BI/BRI-"OLX__LV?OJWO`OXV5P[LH7/XUNXLWXU&lt;@"V^0X&gt;W^(&gt;_Y_DOT=@&lt;P^Y9X,(&lt;&lt;`ZX"H@`OC-2\][$(RM@"G0ND`(NXT@&lt;$_./WB[V7FL\NZ,4_(&gt;K'@;(LXG7;0@R&lt;S+#A!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">mysql</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.11</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"DG5F.31QU+!!.-6E.$4%*76Q!!&amp;,1!!!1S!!!!)!!!&amp;*1!!!!?!!!!!ANN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!!!!!#A&amp;Q#!!!!Q!!!)!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!*$(`V8'20Z,C@ET(M8ZHK-!!!!-!!!!%!!!!!#NQ'I=&gt;'-$4*RH,7%8/WH_V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!BW"'5T8[)U+,&amp;#L^==81=Q%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"!CS4;X0,W/2X;5&amp;JH&lt;D#H\!!!!"!!!!!!!!!#4!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#2%Z711!!!!!%#EVZ5X&amp;M,E2B&gt;'%)/#YQ,D%V,D!(&lt;G6V&gt;(*B&lt;""D.49Y.W:D/$AZ.DFD.$2E!&amp;"53$!!!!!0!!%!!1J.?6.R&lt;#Z%982B!!!!!!"16%AQ!!!!$Q!"!!%+48F4=7QO2'&amp;U91!$!!!!!!)!!1!!!!!!*A!!!#:YH'0A:'"K9,D!!-3-1-T5Q01$S0Y!YD-)=%"F'!$(^!LR!!!!!!")!!!"'(C=9W$!"0_"!%AR-D!Q81$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(&amp;1.V!^-:Y$Y",IZT&amp;D-"A"`[3AC!!!!$!!"6EF%5Q!!!!!!!Q!!!9=!!!,M?*RL9'2AS$3W-,M!J*G"7*SBA3%Z0S76CQ():Y#!+UQ-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;=S]!+"J(!U6SAQF,)9(IAY@&lt;T"BB&amp;A--T)+V8[A=JZOE)W.-E$H&gt;I?I#(2/6!#R'+S:_+==Y.^W9A@)!(\8AU!L8%$OZ1!K&amp;TDYE+7\51-IU4M22!+&amp;?$J$/#3/OX$IC!(ZD#=[A=\LZ)(ZAA0OPD#1!36!7UR!0G%"G1V7U]VWX%%$\#](%1C6!;%K)&amp;1"C.I"$L%D(('(Y?'Z^P7^8;"Q:E-+9Q=A&lt;A"C5"QD9TU'2A;1B5R!MB;KVA&lt;):I++Q?)+R0Y!:7MA[2&amp;B2*A0UA/3/1.6"W*@AL)&lt;I/Y"C=E#^5S!MF7!\!1I7RP)0A"F'Q(:!F#W*3/9!7&lt;&lt;1&gt;E8I0&lt;CIJX^86S2P!^0PQ$[CHM5!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!```````````X6D.`]F8N@`6O&lt;8`X&lt;[N`^WRV(```````````A!!!!9!"A!'!"_!"A"`Y!9"``A'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``Y9"```G!(``BA!@`Q9!"`Q'!!$Q"A!!!!@````]!!!)!``````````````````````:G:G:G:G:G:G:G:G:G:G`W:A:G"A9'9!"G!'9':G:P^G9!9!9'"A:G9'9'"G:G&lt;`:G"A9'9':A"G"G"A:G:G`W:A:G"G"G:G"A9'9':G:P^G9':A:A:A!':A9'!!:G&lt;`:G:G:G:G:G:G:G:G:G:G``````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!'9!!!!!!!!!`Q!!!!!!!')C*A!!!!!!!0]!!!!!!')G:G)G!!!!!!$`!!!!!')G:G:G9C9!!!!!`Q!!!!)G:G:G:G:C)!!!!0]!!!!#*G:G:G:G&lt;S!!!!$`!!!!!C)G:G:G&lt;`]A!!!!`Q!!!!)C)C:G&lt;```)!!!!0]!!!!#)C)C)P```S!!!!$`!!!!!C)C)C````]A!!!!`Q!!!!)C)C)P````)!!!!0]!!!!#)C)C,````S!!!!$`!!!!!C)C)C````]A!!!!`Q!!!!)C)C)P````)!!!!0]!!!!#)C)C,```]C``!!$`!!!!!#)C)C``]C,```]!`Q!!!!!!)C)P]C,```]!!0]!!!!!!!!C)C&lt;````Q!!$`!!!!!!!!!#&lt;````Q!!!!`Q!!!!!!!!!!$``Q!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````LK[OLK[OLK[OLK[OLK[OLK[OLK[OLK[OLK[OLK[O``_OLKY!LK[O!+Y!LA#OLA!!!+[O!!#OLA#OLK[OLK\``[[OLA!!LA!!LA#O!+Y!LK[OLA#OLA#O!+[OLK[OLP``LK[O!+Y!LA#OLA#OLKY!!+[O!+[O!+Y!LK[OLK[O``_OLKY!LK[O!+[O!+[OLK[O!+Y!LA#OLA#OLK[OLK\``[[OLA#OLKY!LKY!LKY!!!#OLKY!LA#O!!!!LK[OLP``LK[OLK[OLK[OLK[OLK[OLK[OLK[OLK[OLK[OLK[O`````````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!#OLA!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!#OV+`5V+Y!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!#OV+_OLK[OV.3O!!!!!!!!!!!!!0``!!!!!!!!!!#OV+_OLK[OLK[OLN45LA!!!!!!!!!!``]!!!!!!!!!V+_OLK[OLK[OLK[OLK\5V!!!!!!!!!$``Q!!!!!!!!#PL[[OLK[OLK[OLK[OLN&lt;5!!!!!!!!!0``!!!!!!!!!+`5V+_OLK[OLK[OLN&lt;7VK]!!!!!!!!!``]!!!!!!!!!L^45V.3PLK[OLN&lt;7VN&lt;7LQ!!!!!!!!$``Q!!!!!!!!#PV.45V.45L^47VN&lt;7VN;P!!!!!!!!!0``!!!!!!!!!+`5V.45V.45VN&lt;7VN&lt;7VK]!!!!!!!!!``]!!!!!!!!!L^45V.45V.47VN&lt;7VN&lt;7LQ!!!!!!!!$``Q!!!!!!!!#PV.45V.45V.&lt;7VN&lt;7VN;P!!!!!!!!!0``!!!!!!!!!+`5V.45V.45VN&lt;7VN&lt;7VK]!!!!!!!!!``]!!!!!!!!!L^45V.45V.47VN&lt;7VN&lt;7LQ!!!!!!!!$``Q!!!!!!!!$5V.45V.45V.&lt;7VN&lt;7VN45L+SM!!!!!0``!!!!!!!!!!#PL^45V.45VN&lt;7VN45L[SML+SML!!!``]!!!!!!!!!!!!!L^45V.47VN45L[SML+SML!!!!!$``Q!!!!!!!!!!!!!!!+`5V.45LKSML+SML+Q!!!!!!0``!!!!!!!!!!!!!!!!!!#PLKSML+SML+Q!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!+SML+Q!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!)5!!5:13&amp;!!!!!"!!*%4E2"!!!!"!J.?6.R&lt;#Z%982B#$AO-#YR.3YQ"WZF&gt;82S97Q19T5W/$&gt;G9TAY/49Z9T1U:!"16%AQ!!!!$Q!"!!%+48F4=7QO2'&amp;U91!!!!!!!))!!!!!!!!!!1!!!%216%AQ!!!!$Q!"!!%+48F4=7QO2'&amp;U91!$!!!!!!!#[A!!"@BYH+W54UA552T(@W_;D&lt;?C_&amp;&lt;8&gt;&amp;(:47:.1MG%UC24]BE9)M7KK9&gt;K=4=4&lt;$@&gt;.;K$8I:!Q5O2B]#LVQ10H9OFSRTSV#%0G^)RK#"J17?HXZNV:`;0L:@W]"C7X_@X?`0^@2A!RW^7*[6B51@#^P&amp;B3)?SE%9!%GU5DH\.,Y#.EA-A&lt;A`2I9_/MBUJ42JU+!^JT&lt;3&gt;,].0L$;_'C]FBR2AOVB[GHGQ7:E/F3'NVH6434,F&lt;9/S\-BWL92[NEL3ULDC`5.8V3A/",6&amp;H+YWEA&lt;#T]KSWH1L'!GLCPD8W59^:EOH$IRL&amp;@.+]BRWR.%@T*&lt;3JO37XG&gt;&lt;!L:MA;WN,2NS:;"G]RJ&gt;S%BOJ0T3:AGGCGO.-38:;D*F*I.T`.EZX*N_*?YOI%+UGGM-5?3?(LWS9,/=XG&gt;S?XN\S/&amp;ZR$X5Q;UELV-0X1V^=64=6N]"!:+Y2YV&amp;9U4QLG'R"&lt;/W#B@B\S9MA-]"(=[LGH1@:$&amp;,QG%$M'+O1=[OY;J9QQVT$5[_T-$?Q]I*?_"SIKGR@X9B&amp;A`0_[)0@&amp;/TQ6D-^XB_ZEEQ(P;&amp;AP&amp;A]9:[O/&lt;J&amp;']PBJFS1$8)U!O4O7F(97.D!Q0!UU;P)6KL*#WO%FPXQVUL]:#&gt;O*BK*^?,S@(5S,2)T^]NZ4P,&lt;7=\U&amp;E8+\R7HL/8`L_TFV'FR1*H91R;9&lt;K%@ZU:S(97[Q&amp;[9+Q%UY8-5K[TS)QB-XWSMV?+H"6MA&lt;.L;WNZ(#&lt;@&lt;4EL%Z*R6NUW$IV$97\+_!7&gt;%-YR&gt;R48Q&lt;6WI;$R!W@5YYQ]6_]=[SL"`/X,P0[XL/T2M^D=L']K'IG%J_)TU5DR.M;Z6H&gt;232)=9$JG(-#&amp;\(+Z^VN(=;Q4';0.=J=:4)U6[0&gt;D!]6A*D%9`Q4Y*Y'H+O9S9JZ#-;H(/:ST1LLT==([VN+!QIV3"7Q!GS`J5-0\KT[R11SOH!\3!&lt;;/?K0J:_A[X5^]NL\'C3',6,@B/8:]Y[6V@Q'D%,/V!!!!!!!%!!!!*Q!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!0&lt;!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!$0&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!#T!!!!!A#61(!!'!!!!!%!!!"V48F4=7QO2'&amp;U93Z.?6.R&lt;%.M;76O&gt;#Z.?6.R&lt;%.P&lt;GZF9X2J&lt;WYM)%VZ5X&amp;M,E2B&gt;'%M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVD.49Y.W:D/$AZ.DFD.$2E%'VZ=X&amp;M)'.P&lt;GZF9X2J&lt;WY!!":!5!!"!!!.&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.C&lt;4"1!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!W*N-&amp;!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!$0&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!#T!!!!!A#61(!!'!!!!!%!!!"V48F4=7QO2'&amp;U93Z.?6.R&lt;%.M;76O&gt;#Z.?6.R&lt;%.P&lt;GZF9X2J&lt;WYM)%VZ5X&amp;M,E2B&gt;'%M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVD.49Y.W:D/$AZ.DFD.$2E%'VZ=X&amp;M)'.P&lt;GZF9X2J&lt;WY!!":!5!!"!!!.&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!PR=!A!!!!!!#!*6!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M1W^O&lt;G6D&gt;'FP&lt;CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'11&lt;8FT=7QA9W^O&lt;G6D&gt;'FP&lt;A!!&amp;E"1!!%!!!VN?8.R&lt;#ZM&gt;G.M98.T!!%!!1!!!!!!!!!!!!!!!!1!!Q!,!!!!"!!!!&amp;I!!!!I!!!!!A!!"!!!!!!2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!65!!!+(?*T&amp;5=N+QU!50?V5_^;K6&gt;Q)MX)B)1D7WCY+A9I&lt;&amp;9J+V[;4C14(J%VGCNU*@I-L@]T0U$`Q*GXNQIU\/&gt;S:=R`=O?=/A$W]/70M!]C2G?P:\54:Z[ZW\9TW63"$P?"2'%KBASCU_+L/YE-:*R4M(&gt;M:,.YX3JN9^E*J&gt;/QKCQ`-3!8C5M\OIE=:^M2JOX0GCU[HW_[+6MNL0-W3C?,CJT]QR.@(_V742G,6,'OLK1J'^358SEW3]NQ47O()';1#$PL+*&amp;L'00*Z6M,(=4"VN?1?D9IV!CLQ3/QH]O3QB9M4&amp;&amp;"WCG#_?E$*?7(2D:Y(:/3D1&gt;U:VF&amp;%C2H0RSP_?W7I:9)L4$^\*'&amp;!;MKIYP#0;]C$DP8UI/I3\EF(!2@5BG-$GZ2/U;$9%PE&amp;@E&gt;7G;U-]XXGM%V-:(VT:%#&gt;&lt;E90VIDN)0X:*H;*VY&amp;P:L[1NA!!!!!!!&amp;]!!1!#!!-!"!!!!%A!$11!!!!!$1$1!,E!!!"0!!U%!!!!!!U!U!#Z!!!!6A!."!!!!!!.!.!!O1!!!&amp;W!!)1!A!!!$1$1!,E'6'&amp;I&lt;WVB"F2B;'^N91:597BP&lt;7%"-!"35V*$$1I!!UR71U.-1F:8!!!5N!!!"$)!!!!A!!!5F!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!`````Q!!!!!!!!$)!!!!!!!!!!$`````!!!!!!!!!.Q!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!&amp;]!!!!!!!!!!$`````!!!!!!!!!91!!!!!!!!!!0````]!!!!!!!!"M!!!!!!!!!!!`````Q!!!!!!!!(]!!!!!!!!!!$`````!!!!!!!!!AQ!!!!!!!!!"0````]!!!!!!!!$G!!!!!!!!!!(`````Q!!!!!!!!/M!!!!!!!!!!D`````!!!!!!!!!\Q!!!!!!!!!#@````]!!!!!!!!$U!!!!!!!!!!+`````Q!!!!!!!!0A!!!!!!!!!!$`````!!!!!!!!!`1!!!!!!!!!!0````]!!!!!!!!%$!!!!!!!!!!!`````Q!!!!!!!!1A!!!!!!!!!!$`````!!!!!!!!"+1!!!!!!!!!!0````]!!!!!!!!'K!!!!!!!!!!!`````Q!!!!!!!!KM!!!!!!!!!!$`````!!!!!!!!#TA!!!!!!!!!!0````]!!!!!!!!/+!!!!!!!!!!!`````Q!!!!!!!!YQ!!!!!!!!!!$`````!!!!!!!!$DA!!!!!!!!!!0````]!!!!!!!!/3!!!!!!!!!!!`````Q!!!!!!!![Q!!!!!!!!!!$`````!!!!!!!!$LA!!!!!!!!!!0````]!!!!!!!!3G!!!!!!!!!!!`````Q!!!!!!!"+A!!!!!!!!!!$`````!!!!!!!!%KA!!!!!!!!!!0````]!!!!!!!!3V!!!!!!!!!#!`````Q!!!!!!!"1Q!!!!!!FN?8.R&lt;#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!ANN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!-!!%!!!!!!!!!!!!!!1!51&amp;!!!!VN?8.R&lt;#ZM&gt;G.M98.T!!%!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!1!!!!!"!"2!5!!!$7VZ=X&amp;M,GRW9WRB=X-!!1!!!!!!!@````Y!!!!!!!!#%%F%982B9G&amp;T:3ZM&gt;GRJ9H!2352B&gt;'&amp;C98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!``]!!!!"!!!!!!!#!!!!!!1!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!I!"&amp;"P=H1!!".!#A!-&gt;'FN:7^V&gt;#!I&lt;8-J!!";!0(8R;AC!!!!!QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T#7VZ=X&amp;M,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!$````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#%%F%982B9G&amp;T:3ZM&gt;GRJ9H!2352B&gt;'&amp;C98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!``]!!!!"!!!!!!!$!!!!!!1!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!=!"&amp;"P=H1!!".!"Q!-&gt;'FN:7^V&gt;#!I&lt;8-J!!";!0(8R;D2!!!!!QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T#7VZ=X&amp;M,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!$!!!!!!!!!!%!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!"!!!!!!%!"2!-0````]+36!A172E=G6T=Q!!#U!$!!21&lt;X*U!!!41!-!$(2J&lt;76P&gt;81A+'VT+1!!7A$RV]7J&amp;A!!!!-,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=QFN?8.R&lt;#ZD&gt;'Q!,E"1!!-!!!!"!!)&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!$!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!)1352B&gt;'&amp;C98.F,GRW&lt;'FC="&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!$``Q!!!!%!!!!!!!5!!!!!"!!51$$`````#EF1)%&amp;E:(*F=X-!!!N!!A!%5'^S&gt;!!!%U!$!!RU;7VF&lt;X6U)#BN=SE!!&amp;I!]&gt;@&amp;K41!!!!$#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-*&lt;8FT=7QO9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!-!!!!!!!!!!1!!!!)!!!!!!!!!!!!!!!!!!!)1352B&gt;'&amp;C98.F,GRW&lt;'FC="&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!$``Q!!!!%!!!!!!!9!!!!!"!!51$$`````#EF1)%&amp;E:(*F=X-!!!N!"A!%5'^S&gt;!!!%U!$!!RU;7VF&lt;X6U)#BN=SE!!&amp;I!]&gt;@&amp;K5I!!!!$#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-*&lt;8FT=7QO9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!-!!!!!!!!!!1!!!!)!!!!!!!!!!!!!!!!!!!)1352B&gt;'&amp;C98.F,GRW&lt;'FC="&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!$``Q!!!!%!!!!!!!=!!!!!"1!51$$`````#EF1)%&amp;E:(*F=X-!!!N!"A!%5'^S&gt;!!!%U!$!!RU;7VF&lt;X6U)#BN=SE!!"2!=!!&amp;$7.P&lt;GZF9X2J&lt;WYA351!8!$RV]7N11!!!!-,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=QFN?8.R&lt;#ZD&gt;'Q!-%"1!!1!!!!"!!)!!RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!1!!!!%!!!!!!!!!!%!!!!#`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!#!!!!!!-!"2!=!!&amp;$7.P&lt;GZF9X2J&lt;WYA351!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!9!"&amp;"P=H1!!".!!Q!-&gt;'FN:7^V&gt;#!I&lt;8-J!!"A!0%!!!!!!!!!!QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T'7VZ=X&amp;M)'.P&lt;GZF9X2J&lt;WYA;7ZG&lt;SZD&gt;'Q!*%"1!!-!!1!#!!-41W^O&lt;G6D&gt;'FP&lt;C"*&lt;G:P)'^V&gt;!!21!=!#W.M;76O&gt;&amp;^G&lt;'&amp;H!"6!"Q!0&lt;7&amp;Y8X"B9WNF&gt;&amp;^T;8JF!"*!-0````])&gt;8.F=GZB&lt;75!!"*!-0````])='&amp;T=X&gt;P=G1!!"*!-0````]):'&amp;U97*B=W5!!&amp;]!]1!!!!!!!!!$#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-79WRJ:7ZU)'FO:G]A='&amp;D;W6U,G.U&lt;!!G1&amp;!!"1!&amp;!!9!"Q!)!!E21WRJ:7ZU)&amp;"B9WNF&gt;#"P&gt;81!7A$RV]FN31!!!!-,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=QFN?8.R&lt;#ZD&gt;'Q!,E"1!!-!!!!%!!I&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!,!!!!!Q!!!!0``````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)1352B&gt;'&amp;C98.F,GRW&lt;'FC="&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!$``Q!!!!%!!!!!!!E!!!!!!A#61(!!'!!!!!%!!!"V48F4=7QO2'&amp;U93Z.?6.R&lt;%.M;76O&gt;#Z.?6.R&lt;%.P&lt;GZF9X2J&lt;WYM)%VZ5X&amp;M,E2B&gt;'%M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVD.49Y.W:D/$AZ.DFD.$2E%'VZ=X&amp;M)'.P&lt;GZF9X2J&lt;WY!!&amp;9!]&gt;@/T(Y!!!!$#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-*&lt;8FT=7QO9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!@````]!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!#A!!!!!#!*6!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M1W^O&lt;G6D&gt;'FP&lt;CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'11&lt;8FT=7QA9W^O&lt;G6D&gt;'FP&lt;A!!6A$RV]\-@A!!!!-,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=QFN?8.R&lt;#ZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````A!!!!!!!!!#$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!%!!!!!!!M!!!!!!A#61(!!'!!!!!%!!!"V48F4=7QO2'&amp;U93Z.?6.R&lt;%.M;76O&gt;#Z.?6.R&lt;%.P&lt;GZF9X2J&lt;WYM)%VZ5X&amp;M,E2B&gt;'%M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVD.49Y.W:D/$AZ.DFD.$2E%'VZ=X&amp;M)'.P&lt;GZF9X2J&lt;WY!!&amp;9!]&gt;C&lt;4"1!!!!$#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-*&lt;8FT=7QO9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!@````]!!!!!!!!!!A^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.10</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"]!!!!!A^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X.16%AQ!!!!4A!"!!I!!!!-:X"N8X"B9WNB:W6T!U"D=QBE982B9G&amp;T:1:4&lt;X6S9W5*352B&gt;'&amp;C98.F#5F%982B9G&amp;T:2&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Item Name="mysql.ctl" Type="Class Private Data" URL="mysql.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="iDatabase" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Query" Type="Folder">
			<Item Name="Query with 1D 64bit Integer Result.vi" Type="VI" URL="../Query with 1D 64bit Integer Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!51%!!!@````]!"1:3:8.V&lt;(1!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*)!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 1D Double Result.vi" Type="VI" URL="../Query with 1D Double Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4H6N:8*J9Q!51%!!!@````]!"1:3:8.V&lt;(1!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*)!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 1D Integer Result.vi" Type="VI" URL="../Query with 1D Integer Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(4H6N:8*J9Q!51%!!!@````]!"1:3:8.V&lt;(1!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*)!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 1D String Result.vi" Type="VI" URL="../Query with 1D String Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!51%!!!@````]!"1:3:8.V&lt;(1!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*)!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 2D 64bit Integer Result.vi" Type="VI" URL="../Query with 2D 64bit Integer Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!91%!!!P``````````!!5'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 2D Double Result.vi" Type="VI" URL="../Query with 2D Double Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4H6N:8*J9Q!91%!!!P``````````!!5'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 2D Integer Result.vi" Type="VI" URL="../Query with 2D Integer Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(4H6N:8*J9Q!91%!!!P``````````!!5'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with 2D String Result.vi" Type="VI" URL="../Query with 2D String Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!P``````````!!5'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with no type.vi" Type="VI" URL="../Query with no type.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!5Q&gt;798*J97ZU!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!J!5Q25?8"F!!!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!AA!!!#3!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268967952</Property>
			</Item>
			<Item Name="Query with Scalar 64bit Integer Result.vi" Type="VI" URL="../Query with Scalar 64bit Integer Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with Scalar Double Result.vi" Type="VI" URL="../Query with Scalar Double Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with Scalar Integer Result.vi" Type="VI" URL="../Query with Scalar Integer Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
			<Item Name="Query with Scalar String Result.vi" Type="VI" URL="../Query with Scalar String Result.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#3!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
		</Item>
		<Item Name="Close Connection.vi" Type="VI" URL="../Close Connection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#'VZ=X&amp;M)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">50331776</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972048</Property>
		</Item>
		<Item Name="Execute Statement.vi" Type="VI" URL="../Execute Statement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'1!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].5'&amp;S97VF&gt;'6S=S"J&lt;A!91%!!!P``````````!!5'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!&amp;E"!!!(`````!!5*6G&amp;M&gt;76T)'FO!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'E"!!!(`````!!5.5'&amp;S97VF&gt;'6S=S"J&lt;A!:1!A!%F.U982F&lt;76O&gt;#"J&lt;G2F?#"J&lt;A!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!A!"!!*!!I!#Q!-!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!##!!!!!!!!!!+!!!##!!!!!A!!!#3!!!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1124073600</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972048</Property>
		</Item>
		<Item Name="List Columns.vi" Type="VI" URL="../List Columns.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'"!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!"B!1!!"`````Q!&amp;#U.P&lt;(6N&lt;H-A&lt;X6U!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1%!!!P``````````!!501W^M&gt;7VO)'FO:G]A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'%!Q`````Q^T9WBF&lt;7%O&gt;'&amp;C&lt;'5A;7Y!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!#!!%!!1!"!!*!!1!#A!,!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!E!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="List Databases.vi" Type="VI" URL="../List Databases.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!":!1!!"`````Q!&amp;#72B&gt;'&amp;C98.F=Q!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$$`````"U.P&lt;H2F?(1!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="List Tables.vi" Type="VI" URL="../List Tables.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!"2!1!!"`````Q!&amp;"H2B9GRF=Q!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!*&lt;8FT=7QA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E!Q`````QBE982B9G&amp;T:1!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="Metadata - database.vi" Type="VI" URL="../Metadata - database.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!C1%!!!P``````````!!522'&amp;U97*B=W5A&lt;76U972B&gt;'%!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!*&lt;8FT=7QA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;%!Q`````QN%982B9G&amp;T:3"J&lt;A!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="Metadata - table.vi" Type="VI" URL="../Metadata - table.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!C1%!!!P``````````!!522'&amp;U97*B=W5A&lt;76U972B&gt;'%!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!*&lt;8FT=7QA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;%!Q`````QN%982B9G&amp;T:3"J&lt;A!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="Open Connection.vi" Type="VI" URL="../Open Connection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]21W^O&lt;G6D&gt;'FP&lt;C"T&gt;(*J&lt;G=!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!1I!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972544</Property>
		</Item>
		<Item Name="Prepare Statement.vi" Type="VI" URL="../Prepare Statement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#!!(4H6N:8*J9Q!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;.U982F&lt;76O&gt;#"J&lt;A!!,E"Q!"Y!!"M,&lt;8FT=7QO&lt;(:M;7).&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!)&lt;8FT=7QA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!1I!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1124073600</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972032</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Data Reader Types" Type="Folder">
			<Item Name="Mysql data reader 1D double.vi" Type="VI" URL="../private/Mysql data reader 1D double.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*2W6U5X2S;7ZH!"*!1!!"`````Q!&amp;"5&amp;S=G&amp;Z!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader 1D I64.vi" Type="VI" URL="../private/Mysql data reader 1D I64.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"!!*2W6U5X2S;7ZH!"*!1!!"`````Q!&amp;"5&amp;S=G&amp;Z!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268973584</Property>
			</Item>
			<Item Name="Mysql data reader 1D integer.vi" Type="VI" URL="../private/Mysql data reader 1D integer.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!!Q!*2W6U5X2S;7ZH!"*!1!!"`````Q!&amp;"5&amp;S=G&amp;Z!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader 1D string.vi" Type="VI" URL="../private/Mysql data reader 1D string.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````]*2W6U5X2S;7ZH!"*!1!!"`````Q!&amp;"5&amp;S=G&amp;Z!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8389648</Property>
			</Item>
			<Item Name="Mysql data reader 2D double.vi" Type="VI" URL="../private/Mysql data reader 2D double.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*2W6U5X2S;7ZH!":!1!!#``````````]!"16"=H*B?1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!*.!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M2'&amp;U96*F972F=CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'1/,EZ&amp;6#"3:7:O&gt;7UA;7Y!!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader 2D I64.vi" Type="VI" URL="../private/Mysql data reader 2D I64.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"!!*2W6U5X2S;7ZH!":!1!!#``````````]!"16"=H*B?1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!*.!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M2'&amp;U96*F972F=CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'1/,EZ&amp;6#"3:7:O&gt;7UA;7Y!!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader 2D integer.vi" Type="VI" URL="../private/Mysql data reader 2D integer.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!!Q!*2W6U5X2S;7ZH!":!1!!#``````````]!"16"=H*B?1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!*.!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M2'&amp;U96*F972F=CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'1/,EZ&amp;6#"3:7:O&gt;7UA;7Y!!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader 2D string.vi" Type="VI" URL="../private/Mysql data reader 2D string.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````]*2W6U5X2S;7ZH!":!1!!#``````````]!"16"=H*B?1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!*.!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M2'&amp;U96*F972F=CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'1/,EZ&amp;6#"3:7:O&gt;7UA;7Y!!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8389648</Property>
			</Item>
			<Item Name="Mysql data reader Scalar double.vi" Type="VI" URL="../private/Mysql data reader Scalar double.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*2W6U5X2S;7ZH!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader Scalar I64.vi" Type="VI" URL="../private/Mysql data reader Scalar I64.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"!!*2W6U5X2S;7ZH!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268973584</Property>
			</Item>
			<Item Name="Mysql data reader scalar integer.vi" Type="VI" URL="../private/Mysql data reader scalar integer.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!!Q!*2W6U5X2S;7ZH!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268968976</Property>
			</Item>
			<Item Name="Mysql data reader Scalar string.vi" Type="VI" URL="../private/Mysql data reader Scalar string.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;C!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````]*2W6U5X2S;7ZH!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!EU"Q!"A!!!!"!!!!&gt;5VZ5X&amp;M,E2B&gt;'%O48F4=7R$&lt;'FF&lt;H1O48F4=7R%982B5G6B:'6S,#".?6.R&lt;#Z%982B,#"7:8*T;7^O04!O-#YQ,D!M)%.V&lt;(2V=G5^&lt;G6V&gt;(*B&lt;#QA5(6C&lt;'FD3W6Z6'^L:7Y^9T5W/$&gt;G9TAY/49Z9T1U:!YO4E65)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268973584</Property>
			</Item>
		</Item>
		<Item Name="Flexible Query.vim" Type="VI" URL="../Flexible Query.vim">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'-!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5G6T&gt;7RU!!!91%!!!P``````````!!5'5G6T&gt;7RU!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!FN?8.R&lt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$$`````"V2Z='5A;7Y!'%"!!!,``````````Q!*"V2Z='5A;7Y!&amp;E!Q`````QR2&gt;76S?3"T&gt;(*J&lt;G=!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#'VZ=X&amp;M)'FO!!"B!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!+!!M!$!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!AA!!!))!!!!#A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519040</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
		</Item>
		<Item Name="ping connection and reopen if needed.vi" Type="VI" URL="../private/ping connection and reopen if needed.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#'VZ=X&amp;M)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342713856</Property>
		</Item>
	</Item>
	<Item Name="tests" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="1d query test.vi" Type="VI" URL="../../db layer/tests/1d query test.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="basic functionality test (multiple functions).vi" Type="VI" URL="../../db layer/tests/basic functionality test (multiple functions).vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082655248</Property>
		</Item>
		<Item Name="basic query test.vi" Type="VI" URL="../../db layer/tests/basic query test.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="no result query test.vi" Type="VI" URL="../../db layer/tests/no result query test.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="test prepared statement.vi" Type="VI" URL="../../db layer/tests/test prepared statement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
	</Item>
	<Item Name="construct mysql.vi" Type="VI" URL="../../construct mysql.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&amp;!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!&lt;#WVZ=X&amp;M,GRW&lt;'FC$7VZ=X&amp;M,GRW9WRB=X-!#7VZ=X&amp;M)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]21W^O&lt;G6D&gt;'FP&lt;C"T&gt;(*J&lt;G=!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!1$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!"#A!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Mysql command.vi" Type="VI" URL="../private/Mysql command.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!*&amp;!=!!9!!!!!1!!!(6.?6.R&lt;#Z%982B,EVZ5X&amp;M1WRJ:7ZU,EVZ5X&amp;M2'&amp;U96*F972F=CQA48F4=7QO2'&amp;U93QA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07-V.DAX:G-Y/$EW/7-U.'1.28BF9X6U:6*F972F=A!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!O1(!!(A!!'QNN?8.R&lt;#ZM&gt;GRJ9AVN?8.R&lt;#ZM&gt;G.M98.T!!BN?8.R&lt;#"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!"#A!!!!A!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
</LVClass>
