# README #

**composed-database-mysql** is an extension to @cs/database that adds methods for managing a connection to a mysql database

### How do I get set up?
- Install this package using [GPM](https://gpackage.io) and start coding
- All public API methods are clearly marked in the source library

#### Examples
- No examples provided at this time

#### Dependencies
- This package depends on @cs/database as it implements the interface
- This package depends on the [mysql connector/.NET](https://dev.mysql.com/downloads/connector/net/) 

### Contribution guidelines
- All contributions must adhere to SOLID design principles.
- All code must be unit tested to the extent reasonably possible.
- Please contact the author if you want to contribute.

### Who do I talk to?
- Steve Ball | Composed Systems, LLC
- Steve.Ball@composed.io

### License
- See license file