﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Test mysql.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Test mysql.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"?$5F.31QU+!!.-6E.$4%*76Q!!%TA!!!2,!!!!)!!!%RA!!!!I!!!!!B"5:8.U)'VZ=X&amp;M,GRW&lt;'FC%F2F=X1A&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!!!+!8!)!!!$!!!!A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!"!!"!!&lt;`````!!!!!!!!!!!!!!!!&gt;115ORA6&amp;U7%B/Y$&gt;;S+/!!!!!Q!!!!1!!!!!,__,%H,`6R$HG?00HMSEZP5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#FJFY(VV244+940)C$^Z%7!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%$6T?U^L[J`_=0RR:29&lt;0TY!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!B!!!!'(C=9W"D9'JAO-!!R)Q/4!V-'5$7"Y9!"A!`I177!!!!!!!!2A!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\,#B)(OXA/EG5"S5$610T#&gt;!?)4[/;Q9T%&lt;!(`T+#5!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!'%!!!$&amp;(C=;W"E9-AUND#\!+3:A6C=I9%B/4]FF9M"S'?!A"&gt;-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#JP^-JV!UY#U"*)&amp;=!?AK?=!/F,AY%/7\E9.),^X)IA%#P&amp;UBH")((@BU"%$]BF0A!TMZ)(ZAA0OPD#1!35K!JUG))N9''%7&gt;&lt;-&gt;&gt;^!!_]N""%*F1+A+#&amp;5!IH;!88#%)_YQ0$T8PL[X#R1/&lt;%BB\!$%$5!-CG.EL-@!S!#SE!F)VE,6WA$:4&amp;!R7&amp;S"W"_A&lt;!UE03,1I'2EM)?,L5;SEQEMR]BQBA'GDJ(B%J4&gt;!(5D3%Q7K(E#F+U#:#&gt;!W&gt;J!^A%IWQD)&amp;I#S,2H"$$$&lt;$MK_!(5,,NL:X]56+5D!;2K7L$G"/$GXQ-"!LTI9&amp;!Y!GQ/#`Q!!!)]!!!$=?*RT9'"AS$3W-'M!UMS-$!TC$!U-S@EJK1R)9!M$&lt;N$]2K$&lt;257EUU7&amp;J^N(2;,42Y7DG['&lt;%UCT&gt;,+IP0DT````ZB_-`&amp;-/]G]\S?^SIP.%]R'"QUD[V\[_NQNI,1-DEJA$%#MR3)$&amp;G)"9(EE="*T^86T2X1&gt;3SQH%S&lt;E&amp;"A:[V='V1$9!9K)D\A!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!=2C0A(NXX`0\/&gt;`T_X\@]`M2X`0```_-!!!"D!!!!9Q%!!'!#]!"A")9!9!C!Y'!1A'"A))$A9'#"I'"]A;"A@Y-A9(`W)'"``#"A@`QA9(``)'"``?"A@`R!9(`]A'"``9"A0`E!9!@[!'!!@!"A!!A!9!!!!(`````!!!#!0`````````````````````Q!!!!$`]!$Q!0]!$Q!0``]!!!!!``]0]0`Q```Q``````!0````$`!0`Q$`]0`````Q$````Q`Q````$`$`````]!````]0]!$Q!0`Q``````!0`````````````````Q!#)!!!!!!!!!!!!!!!!0]!!C!!!!!!!!!!!!!!!!$`!!)A!!!-,-!!!!!!!!!!`Q!!!!!!TG:G,-!!!!!!!0]!!!!!$#:G:G:CT!!!!!$`!!!!!-*G:G:G:G&lt;N!!!!`Q!!!!!G:G:G:G:G9A!!!0]!!!!/:G:G:G:G:O9!!!$`!!!!ZG:G:G:G:GZG!!!!`Q!!!/\O:G:G:G&lt;G:A!!!0]!!!$G:G\G:G:O:G9!!!$`!!!!*G:G:OZGZG:G!!!!`Q!!!#:G:G:G9G:G:A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:GQ!!!$`!!!!*G:G:G:O:G&lt;!!!!!`Q!!!':G:G:G&lt;G:C!!!!!0]!!!$.ZG:G:GZG)!!!!!$`!!!!!!T?:G:O9M!!!!!!`Q!!!!!!!-XG&lt;GQ!!!!!!0]!!!!!!!!!$.,!!!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````S1E*#1E*#1E````!!!!`Q!!!0``!!!!`Q!!!0``````*#1E*#1E*#4`````!0``!0```Q$``````Q$```````````]E*0````````]!``]!!0```Q!!````!0```````````S1E`````````Q$``Q$```````]!``]!````````````*#4`````````!0``!!!!`Q!!!0```Q$```````````]E*0```````````````````````````````````Q!!!.04!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!U^-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!$4UQ!!!!!!!0;D?@A!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!$WRXCCH-;&gt;6@9!!!!!!!!!!!!!!0``!!!!!!!!!!!!^K.Y?+*Y?(BYRK.Z+Q!!!!!!!!!!``]!!!!!!!!!!0;D?(BYIHBY?(BY?(C=RY!!!!!!!!$``Q!!!!!!!!!!IXBY?(CC?(BY?(BY?(C=TQ!!!!!!!0``!!!!!!!!!+2Y?(BY?+*Y?(BY?(BYH-@'!!!!!!!!``]!!!!!!!#FH(BY?(BYIHBY?(BY?*T(?-9!!!!!!!$``Q!!!!!!!-@(R]?=?(CC?(BY?(BYRZRYRA!!!!!!!0``!!!!!!!!RZS=H-&lt;(R]:Y?(BY?-?=?(D'!!!!!!!!``]!!!!!!!#DH*S=H*S=RM@(RHD(H(BY?-9!!!!!!!$``Q!!!!!!!+/=H*S=H++=H*T'TJRY?(BYRA!!!!!!!0``!!!!!!!!IZS=H*SCH*S=H*T(H(BY?(CD!!!!!!!!``]!!!!!!!#DH*S=IJS=H*S=H-?CIK*Y?+-!!!!!!!$``Q!!!!!!!+/=H++=H*S=H*S=RZRYIK+CIQ!!!!!!!0``!!!!!!!!IZSCH*S=H*S=H*T(H(BY?*RZ!!!!!!!!``]!!!!!!!#DIJS=H*S=H*S=H-?=?(C=?1!!!!!!!!$``Q!!!!!!!++=H*S=H*S=H*S=RZRYH+-!!!!!!!!!!0``!!!!!!!!_)$(IJS=H*S=H*T(?(CD!!!!!!!!!!!!``]!!!!!!!!!!!$YA-@'H*S=H-&gt;YI`9!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!0B[R];=R]9L!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!+XKL+Q!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!$2!!!"UVYH+W64UA552T(@W`=[OW;_-9`[:+WK\QV$QK67;G:EG.EC#6GK:&gt;=(,-&amp;W9X&gt;61."/QSGAK@!1S"%"[]&gt;0.229KVA,Z[#.&amp;C]&gt;KK$'/LM^HMT\O[YF&gt;P"/1S0Y8V`X^`]PJ]X!X#KAZ6+=:D3A&lt;"N8(4KY&amp;#D"#"33_(AKJI"VEPWA"1ZC1ZX;3`&lt;F/+E4)@4;L3+8F$GY-@"&lt;F)P.&lt;-NX(K3/&lt;'91Y&gt;].6ICX_%RRN_7]&lt;E4S;LZ=*9NE,D5RVU\&gt;%%,I#&amp;IV?)OVZ)Y%+8#:N-K\XH^QRI84_WVV'G5N/P!F'B?E-@/9U7U`G#5F'I)2_O$EI!FKW&amp;F:35NEEV2F&gt;('.&gt;11&lt;L2&lt;=Y3G1)G7BXCMRN!Y$!X[V#&gt;^&amp;&amp;@]J?B&gt;C$+FB5K5I22VTZ+DY5EPR;7X`F8X2)=C(LN*H82,X3"ZX&gt;I\)%!CDWDC@7*-[/5OE9+RNQ#$]$13VI0L(BUO;F&amp;J%'T*]&lt;&lt;!P"'$,2F$H9DBFB'$8:FDE-ZB0EM/CCV37&gt;YW/B9+$Q@&gt;A=@OI6&amp;P+/2_'P3.?]0$&lt;N5&lt;^JI*-9`:'[K!6R.]#9]VN-N+V(F6$%4YG\Q5AAW;9.);1!#7FZ&gt;R*HB03_N27M*D+6W_]9,4K2$5^$"&amp;)_FB8M&amp;B?PL"-Q$+L`MD9KS?2OEQT,@4-$=AT$,,&lt;/Y1T%X(!X/756V(\+9S_)9*K)&lt;:)VBN.E6JPH%`1!.-(+'ZA:JJ+^_IG5$.&lt;(;_7`\A7WAT_&amp;Z=8-TEOT56C5GZD2#4=GU^M:`9&amp;[TP*&lt;Z$(&lt;SQM.[./;'@A$&lt;R%ZW=[(1IF^Z`UDXR(X1\P?ZR\[B0&gt;1]&amp;`0\BI&lt;!PY(?(QE'@@S2\8,E[0%!]'XEMX`3GQLM9Y;E!"Y\A[]9X(-(9T'P::2Z(=U!\HY8_90X*MPZI7;^2J^W6^HFI_IS&lt;0KN*3,HBAQ\(YI0$\%/@3TS7;TGJR9&lt;09$,?B'T%O\O\CZZY.Y?J6="TZ*S,*VE)\]]YU&lt;+"DZL^2!^94`3;:V)S$X6/V5Q/PE38B87[_8%M^1/D06R*(,7"N70^;2W+F&lt;;#6&gt;9BGK=&gt;N*UNY;="PR*H[",&gt;DHR*`?)CH3GFNE\/37`Y+R=N`1X5&lt;!)E!!!!"!!!!#E!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"F!!!!&gt;8C=9W"A+"3190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT```^8+7,E_(LE'FT2%2]Y5W770)=%!'5)':I!!!!!!!!%!!!!"Q!!!J1!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!')8!)!!!!!!!1!)!$$`````!!%!!!!!!%9!!!!#!#*!-0````]:93"W97RJ:#"D&lt;WZO:7.U;7^O)(.U=GFO:Q!=1&amp;!!!1!!%F2F=X1A&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#E8!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!%!!!!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$9F*#"!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.C5E)%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!9B=!A!!!!!!"!!A!-0````]!!1!!!!!!2A!!!!)!)E!Q`````RFB)(:B&lt;'FE)'.P&lt;GZF9X2J&lt;WYA=X2S;7ZH!"R!5!!"!!!36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!5B=!A!!!!!!#!#*!-0````]:93"W97RJ:#"D&lt;WZO:7.U;7^O)(.U=GFO:Q!=1&amp;!!!1!!%F2F=X1A&lt;8FT=7QO&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!!!!!!!%!!-!#!!!!!1!!!";!!!!+!!!!!)!!!1!!!!!+A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%'!!!"G(C=D9`.3M.!&amp;)7`:0K4VL&lt;'KAM8QO$#B1M2@)'!Y,K)$W"-*C5Q*JJ-A_\U$6TY=$['0I(?JB56.]["!`@=SZFTA&amp;U/IJ-0?8OR&lt;G+&lt;JTIJC])E,C],8&lt;MK,_99XF^@HJ]!&amp;6[;WOG&lt;B`L/(NP'ZN@48U*CY\K?`*!3:TG+:HCQ@W98N4/6,D0&gt;XOH&lt;+G^C:X1;OZCOA#%J?'`Y-KDVS#E&gt;"F%@F&gt;EZ1@3ISAOX%ES:%9K\IE?@1#X3D)#W$K0WV[&amp;S^[HYT-2SQ!;(`]TC)^2&lt;EFQ(8%G-$O&gt;CIZGQ+?MF1N'_Y+`R6`H?&lt;,69F@+9CL+M[%H],C0'&lt;,-D0):02`B0+A!!!!!!=1!"!!)!!Q!&amp;!!!!7!!."!!!!!!.!.!!O1!!!&amp;]!$11!!!!!$1$1!,E!!!"G!!U%!!!!!!U!U!#Z!!!!&lt;9!!B!#!!!!.!.!!O1!!!'_!!)1!A!!!$1$1!,E'6'&amp;I&lt;WVB"F2B;'^N91:597BP&lt;7%"-A%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!%TA!!!2,!!!!)!!!%RA!!!!!!!!!!!!!!#!!!!!U!!!%0!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B$1V.5!!!!!!!!!:R-38:J!!!!!!!!!&lt;"$4UZ1!!!!!!!!!=2544AQ!!!!!!!!!&gt;B%2E24!!!!!!!!!?R-372T!!!!!!!!!A"735.%!!!!!1!!!B2W:8*T!!!!"!!!!DR41V.3!!!!!!!!!K"(1V"3!!!!!!!!!L2*1U^/!!!!!!!!!MBJ9WQU!!!!!!!!!NRJ9WQY!!!!!!!!!P"-37:Q!!!!!!!!!Q2'5%BC!!!!!!!!!RB'5&amp;.&amp;!!!!!!!!!SR75%21!!!!!!!!!U"-37*E!!!!!!!!!V2#2%BC!!!!!!!!!WB#2&amp;.&amp;!!!!!!!!!XR73624!!!!!!!!!Z"%6%B1!!!!!!!!![2.65F%!!!!!!!!!\B)36.5!!!!!!!!!]R71V21!!!!!!!!!_"'6%&amp;#!!!!!!!!!`1!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#Q!!!!!!!!!!0````]!!!!!!!!!U!!!!!!!!!!!`````Q!!!!!!!!$E!!!!!!!!!!$`````!!!!!!!!!/Q!!!!!!!!!!0````]!!!!!!!!"'!!!!!!!!!!!`````Q!!!!!!!!%A!!!!!!!!!!$`````!!!!!!!!!5A!!!!!!!!!!0````]!!!!!!!!"F!!!!!!!!!!!`````Q!!!!!!!!'E!!!!!!!!!!(`````!!!!!!!!!SQ!!!!!!!!!"0````]!!!!!!!!$Q!!!!!!!!!!(`````Q!!!!!!!!05!!!!!!!!!!D`````!!!!!!!!!_1!!!!!!!!!#@````]!!!!!!!!$_!!!!!!!!!!+`````Q!!!!!!!!1)!!!!!!!!!!$`````!!!!!!!!""Q!!!!!!!!!!0````]!!!!!!!!%.!!!!!!!!!!!`````Q!!!!!!!!2)!!!!!!!!!!$`````!!!!!!!!"-Q!!!!!!!!!!0````]!!!!!!!!'U!!!!!!!!!!!`````Q!!!!!!!!L5!!!!!!!!!!$`````!!!!!!!!#O1!!!!!!!!!!0````]!!!!!!!!/,!!!!!!!!!!!`````Q!!!!!!!!YU!!!!!!!!!!$`````!!!!!!!!$DQ!!!!!!!!!!0````]!!!!!!!!/4!!!!!!!!!!!`````Q!!!!!!!![Y!!!!!!!!!!$`````!!!!!!!!$M!!!!!!!!!!!0````]!!!!!!!!27!!!!!!!!!!!`````Q!!!!!!!"&amp;A!!!!!!!!!!$`````!!!!!!!!%7A!!!!!!!!!!0````]!!!!!!!!2F!!!!!!!!!#!`````Q!!!!!!!"+A!!!!!!Z5:8.U)'VZ=X&amp;M,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B"5:8.U)'VZ=X&amp;M,GRW&lt;'FC%F2F=X1A&lt;8FT=7QO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!1!!1!!!!!!!!%!!!!#!!J!)12/&lt;WZF!!!)!&amp;!!!1!!!!%!!1!!!!!!!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!))9!#!!!!!!!!!!!!!!%!!!!!!!!#!!!!!A!+1#%%4G^O:1!!#!"1!!%!!!!"!!%!!!!"`````A!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!#!!J!)12/&lt;WZF!!!)!&amp;!!!1!!!!%!!1!!!!(````_!!!!!!%16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!)!)E!Q`````RFB)(:B&lt;'FE)'.P&lt;GZF9X2J&lt;WYA=X2S;7ZH!'5!]&gt;C5E)%!!!!$%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T$F2F=X1A&lt;8FT=7QO9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!@````]!!!!!!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!A!!!"B5:7VQ&lt;'&amp;U:62F=X2$98.F,GRW9WRB=X-!!!!36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"H!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!"*!!!!"A=]&gt;GFM;7)_"G&amp;E:'^O=QV@3EN*)&amp;2P&lt;WRL;82T#6:*)&amp;2F=X2F=AR5:8.U1W&amp;T:3ZM&lt;')16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Test mysql.ctl" Type="Class Private Data" URL="Test mysql.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972434</Property>
	</Item>
	<Item Name="Test_OpenConnection_NoError.vi" Type="VI" URL="../Test_OpenConnection_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_OpenConnectionBadCredentials_ReturnsError.vi" Type="VI" URL="../Test_OpenConnectionBadCredentials_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_OpenConnectionBadDatabase_ReturnsError.vi" Type="VI" URL="../Test_OpenConnectionBadDatabase_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_OpenConnectionBadIP_ReturnsError.vi" Type="VI" URL="../Test_OpenConnectionBadIP_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_OpenConnectionBadPort_ReturnsError.vi" Type="VI" URL="../Test_OpenConnectionBadPort_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQuery_WorksNoError.vi" Type="VI" URL="../Test_SQLQuery_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_ListDatabases_WorksNoError.vi" Type="VI" URL="../Test_ListDatabases_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_ListTables_WorksNoError.vi" Type="VI" URL="../Test_ListTables_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_ListColumns_WorksNoError.vi" Type="VI" URL="../Test_ListColumns_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM2DSTR_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM2DSTR_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM1DSTR_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM1DSTR_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIMSTR_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIMSTR_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIMINT_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIMINT_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIMI64_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIMI64_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIMDBL_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIMDBL_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM2DINT_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM2DINT_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM2DI64_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM2DI64_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM2DDBL_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM2DDBL_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM1DINT_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM1DINT_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM1DI64_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM1DI64_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIM1DDBL_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIM1DDBL_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_SQLQueryVIMBinaryVariant_WorksNoError.vi" Type="VI" URL="../Test_SQLQueryVIMBinaryVariant_WorksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F%&amp;2F=X1A&lt;8FT=7QO&lt;(:M;7)36'6T&gt;#"N?8.R&lt;#ZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#516'6T&gt;#"N?8.R&lt;#ZM&gt;GRJ9B*5:8.U)'VZ=X&amp;M,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
</LVClass>
